#!/bin/bash

password="wuying"

sleep 20

if [ ! -f $HOME/reboot_num.txt ];then
    echo 0 > $HOME/reboot_num.txt
fi

last_num=`cat $HOME/reboot_num.txt`
reboot_num=$(($last_num + 1))

if [ ! -f $HOME/q ] && [ $reboot_num -le 1000 ];then
    echo $reboot_num > $HOME/reboot_num.txt
    echo $password | sudo -S shutdown -r now
fi
